from .base import BaseDAO
from ..models import User
from ..schemas import UserSchema


class UserDAO(BaseDAO):
    model = User
    schemas = {
        'default': UserSchema
    }
