from marshmallow import fields
from marshmallow_sqlalchemy import field_for

from ..addons import ma
from ..models import User
from ..helpers.marshmallow import TitleCaseString, UpperCaseString

class UserSchema(ma.ModelSchema):
    first_name = field_for(User, 'first_name', field_class=TitleCaseString)
    last_name = field_for(User, 'last_name', field_class=UpperCaseString)
    email = field_for(User, 'email', field_class=fields.Email)
    
    class Meta:
        fields = ('id', 'first_name', 'last_name', 'email')
        strict = True
        model = User
        