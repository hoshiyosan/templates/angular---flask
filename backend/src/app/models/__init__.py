from ..addons import db
from .user import User

def create_db(app):
    db.init_app(app)
    
    with app.app_context():
        db.create_all()
