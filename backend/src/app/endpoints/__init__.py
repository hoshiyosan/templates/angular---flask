from ..addons import api
from .auth import ns as auth_ns
from .user import ns as user_ns

def create_api(app):
    api.init_app(app)
    
    api.add_namespace(auth_ns)
    api.add_namespace(user_ns)
